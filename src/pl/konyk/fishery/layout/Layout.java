package pl.konyk.fishery.layout;

import pl.konyk.fishery.classes.Fish;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by Pawel on 24-04-2017.
 */
public class Layout extends JFrame{

    private void appWindow() {


        JFrame frame = setFrame();
        Container cont = frame.getContentPane();

        JPanel menuBar = setMenu();
        menuBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        cont.add(menuBar, BorderLayout.WEST);

        JPanel searchPanel = searchPanel();
        cont.add(searchPanel, BorderLayout.CENTER);

        JPanel editPanel = editPanel();
        cont.add(editPanel, BorderLayout.EAST);

        JPanel resultPanel = resultPanel();
        cont.add(resultPanel, BorderLayout.WEST);

        frame.pack();
        frame.setVisible(true);
    }


    private JFrame setFrame() {

        JFrame frame = new JFrame();
        ImageIcon img = new ImageIcon("out/gfx/app.png");

        frame.setTitle("Fishery");
        frame.setIconImage(img.getImage());

        return frame;
    }


    private JPanel setMenu(){

        JPanel panel = new JPanel();
        JMenuBar menuBar;
        JMenu menu, submenu;
        JMenuItem menuItem;
        JRadioButtonMenuItem rbMenuItem;
        JCheckBoxMenuItem cbMenuItem;

        menuBar = new JMenuBar();

        menu = new JMenu("Pomoc");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription(
                "The only menu in this program that has menu items");
        menuBar.add(menu);

        menuItem = new JMenuItem("A text-only menu item",
                KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription(
                "This doesn't really do anything");

        menu.add(menuItem);
        panel.add(menuBar);
        return panel;
    }


    private JPanel editPanel() {

        JPanel editPanel = new JPanel();
        editPanel.setLayout(new GridLayout(6,2));

        JLabel idLabel = new JLabel("ID: ", SwingConstants.RIGHT);
        JTextField id = new JTextField();

        JLabel nameLabel = new JLabel("Nazwa: ", SwingConstants.RIGHT);
        JTextField name = new JTextField();

        JLabel famidLabel = new JLabel("Grupa ", SwingConstants.RIGHT);
        JTextField famid = new JTextField();

        JLabel countryLabel = new JLabel("Kraj: ", SwingConstants.RIGHT);
        JTextField country = new JTextField();

        JLabel imageLabel = new JLabel("Zdjęcie: ", SwingConstants.RIGHT);
        JTextField image = new JTextField();

        // Etykieta l żeby było równo
        JLabel l = new JLabel();
        JButton change = new JButton("Zmień");

        editPanel.add(idLabel);
        editPanel.add(id);

        editPanel.add(nameLabel);
        editPanel.add(name);

        editPanel.add(famidLabel);
        editPanel.add(famid);

        editPanel.add(countryLabel);
        editPanel.add(country);

        editPanel.add(imageLabel);
        editPanel.add(image);

        editPanel.add(l);
        editPanel.add(change);

        return editPanel;

    }

    private JPanel searchPanel(){

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),"Wyszukiwarka"));
        JLabel textLabel = new JLabel("Wpisz szukaną rybę: ");
        JTextField textField = new JTextField();
        JButton search = new JButton("Szukaj");
        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showConfirmDialog(panel, textField.getText() +  "", "Moje okno" + e.getActionCommand(), JOptionPane.PLAIN_MESSAGE);
                updateFishList(textField.getText());
            }


        });

        textField.setPreferredSize(new Dimension(250, 25));

        panel.add(textLabel);
        panel.add(textField);
        panel.add(search);
        return panel;
    }

    private void updateFishList(String text) {

    }



    private JPanel resultPanel() {

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),"Znalezione"));
        panel.setLayout(new GridLayout(2,2));
        panel.setPreferredSize(new Dimension(200,225));
        JList<Fish> fishList = new JList<Fish>();
        fishList.setVisibleRowCount(10);


        fishList.setVisibleRowCount(15);

        panel.add(fishList);

        return panel;

    }


    public static void main(String[] args) {
        Layout layout = new Layout();
        layout.appWindow();
    }
}
