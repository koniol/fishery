package pl.konyk.fishery.database;

import java.io.IOException;
import java.sql.*;

/**
 * Created by Pawel on 08-04-2017.
 */
public abstract class PgManager
{
    private static final String DB_DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/fish_fishing";
    private static final String DB_USER = "fish_fishing";
    private static final String DB_PASS = "fish_fishing";

    private String sql;
    private String where;
    private Connection c = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    public PgManager(String sql, String where) throws SQLException, ClassNotFoundException {
        try {
            connect();
            prepareQuery(sql, where);
            close();
        }catch (Exception e){
            //@TODO Write trow window. JOptionPane.showConfirmDialog

            System.out.println("Nie można nawiązać połącznia z bazą danych !!!");
            System.out.println(e.getMessage());
        }
    }


    public PgManager(String sql) throws SQLException, ClassNotFoundException {
        try {
            connect();
            executeQuery(sql);
            printData(rs);
            close();
        }catch (Exception e){
            //@TODO Write trow window. JOptionPane.showConfirmDialog

            System.out.println("Nie można nawiązać połącznia z bazą danych !!!");
            System.out.println(e.getMessage());
        }
    }

    protected abstract void printData(ResultSet rs) throws IOException, SQLException;

    public ResultSet getResult() {
        return rs;
    }

    public String getSql() {
            return sql;
        }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public void connect() throws SQLException, ClassNotFoundException  {
            Class.forName(DB_DRIVER);
            c = DriverManager.getConnection(URL, DB_USER, DB_PASS);
            this.c = c;
    }

    public ResultSet prepareQuery(String sql, String where) throws SQLException {
        PreparedStatement preparedStatement = c.prepareStatement(this.sql + " WHERE = ? ");
        preparedStatement.setString(1, this.where);
        ResultSet rs = preparedStatement.executeQuery();
        return rs;
    }


    public void executeQuery(String sql) throws SQLException {
        this.stmt = c.createStatement();
        this.rs = stmt.executeQuery(sql);
    }


    public void close() throws SQLException {
        stmt.close();
        rs.close();
        c.close();
    }
}
