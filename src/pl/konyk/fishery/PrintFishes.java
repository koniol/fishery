package pl.konyk.fishery;

import pl.konyk.fishery.database.PgManager;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Pawel on 15-04-2017.
 */
public class PrintFishes extends PgManager {
    private static final char N_CODE = 110; // n
    private static final char END_CODE = 101; // e

    public PrintFishes(String sql) throws SQLException, ClassNotFoundException {
        super(sql);
    }


    public static void main(String [] args) throws SQLException, ClassNotFoundException, IOException {
        PrintFishes con = new PrintFishes("SELECT * FROM fishes") ;
    }

    @Override
    protected void printData(ResultSet rs) throws SQLException, IOException {


            // Przypisz na start pierwszy element
            char znak = N_CODE;

            while (znak != END_CODE) {
                System.out.println(" e - EXIT    n - NEXT " );
                if (znak == N_CODE) {
                    rs.next();
                    int fish_id = rs.getInt("fishid");
                    String name = rs.getString("f_name");
                    String opis = rs.getString("dscrpt");

                    System.out.println("ID " + fish_id + "\nNazwa: " + name + "\nOpis: " +
                            "" + opis + "\n ------------------------");
                }
                znak = (char) System.in.read();
                if (rs.isLast()) {
                    System.out.println("Nie ma już elementów");
                    znak = END_CODE;
                }

            }
    }
}
